package edu.usyd.au.fitness.domain.recommend;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name="EXERCISE_PLAN")
@SequenceGenerator(name="SEQ_EXERCISE_PLAN", initialValue=100001, allocationSize=100,sequenceName="SEQ_EXERCISE_PLAN")
public class ExercisePlan implements Serializable {

	@Id
	@GeneratedValue
	@Column(name="Id")
	private int id;

	@Column(name="USERID")
	private int user_id;
	
	@Column(name="FOODNAME", length = 20)
    private String foodName;
	
	@Column(name="AMOUNT", length = 20)
    private double amount;
	
	@Column(name="UNIT", length = 20)
    private String unit;
	
	@Column(name="CALS", length = 20)
    private String cals;
	
	@Column(name="FAT", length = 20)
    private String fat;
	
	@Column(name="CARBS", length = 20)
    private String carbs;
	
	@Column(name="PROT", length = 20)
    private String prot;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATEDDATE")
    private Date createdDate;

	
}
