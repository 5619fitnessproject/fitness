package edu.usyd.au.fitness.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;



@Entity
@Table(name="USER")
@SequenceGenerator(name="seq_user", initialValue=100001, allocationSize=100,sequenceName="seq_user")
public class User implements Serializable {
	

	

	@Id
	@GeneratedValue
	@Column(name="Id")
	private int id;
	

	@Column(name="EMAIL", nullable = false, length = 40, unique = true)
	private String email;

	@Column(name="PASSWORD", nullable = false, length = 20)
    private String password;
	
	@Column(name="USER_NAME", length = 20)
    private String userName;
	
	@Column(name="CONFIRMEDPASSWORD", length = 20)
	private String confirmPassword;
	
	@Temporal(TemporalType.DATE)
	@Column(name="CREATEDDATE")
    private Date createdDate;
	
	@OneToMany(mappedBy="user",cascade=CascadeType.ALL)
	private Set<UserFood> userFoodList;

	@OneToMany(mappedBy="user",cascade=CascadeType.ALL)
	private Set<UserWeight> userWeightList;	

	@OneToMany(mappedBy="user",cascade=CascadeType.ALL)
	private Set<UserActivity> userActivityList;


	@OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "planner_Id")
	private Planner planner;

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}
	
	public Set<UserActivity> getUserActivityList() {
		return userActivityList;
	}

	public void setUserActivityList(Set<UserActivity> userActivityList) {
		this.userActivityList = userActivityList;
	}
	
	public Set<UserWeight> getUserWeightList() {
		return userWeightList;
	}

	public void setUserWeightList(Set<UserWeight> userWeightList) {
		this.userWeightList = userWeightList;
	}
	
	public Planner getPlanner() {
		return planner;
	}

	public void setPlanner(Planner planner) {
		this.planner = planner;
	}

	public Set<UserFood> getUserFoodList() {
		return userFoodList;
	}

	public void setUserFoodList(Set<UserFood> userFoodList) {
		this.userFoodList = userFoodList;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
//	@Column(name="HEIGHT", length = 20)
//    private String height;
//	
//	@Column(name="WEIGHT", length = 20)
//    private String weight;
//	
//	@Column(name="AGE", length = 20)
//    private String age;
	
	
	
	
}
