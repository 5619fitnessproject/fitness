package edu.usyd.au.fitness.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.IdClass;
//import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@IdClass(ResultId.class)
@Table(name="RESULT")
@SequenceGenerator(name="seq_user", initialValue=100001, allocationSize=100,sequenceName="seq_user")
public class Result implements Serializable {


	@Id
	@GeneratedValue
	@Column(name="Id")
	private int id;


	@Column(name="USER_NAME", nullable = false,length = 20)
	private String userName;

	@Column(name="DATE",nullable = false, length = 20)
	private Date date;

	@Column(name="SCORE",nullable = false, length = 20)
	private int score;
	
	@Column(name="LEVEL",nullable = false, length = 20)
	private String level;
	
	public String getLevel() {
		return level;
	}
	public void setLevel(String level) {
		this.level = level;
	}
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}

	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
		//System.out.println(this.date.toString());
	}
	
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}	
}
