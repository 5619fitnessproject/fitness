package edu.usyd.au.fitness.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name="REMINDER")
@SequenceGenerator(name="seq_reminder", initialValue=100001, allocationSize=100,sequenceName="seq_reminder")
public class Reminder implements Serializable {

	@Id
	@GeneratedValue
	@Column(name="Id")
	private int id;

//	@Column(name="EMAIL", nullable = false, length = 40, unique = true)
//	private String email;
	
	@Column(name="TIMEINTERVAL", nullable = false, length = 20)
    private String timeInterval;
	
	@Temporal(TemporalType.DATE)
	@Column(name="CREATEDDATE")
    private Date createdDate;
	
//	public String getEmail() {
//		return email;
//	}
//
//	public void setEmail(String email) {
//		this.email = email;
//	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTimeInterval() {
		return timeInterval;
	}

	public void setTimeInterval(String timeInterval) {
		this.timeInterval = timeInterval;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}




	
//	@Column(name="AGE", length = 10)
//    private String age;
//	
//	@Column(name="CURRENT_WEIGHT", length = 10)
//    private String currentWeight;
//	
//	@Column(name="GOAL_WEIGHT", length = 10)
//    private String goalWeight;
//	
//	@Column(name="DURATION", length = 10)
//    private String duration;

//	public int getId() {
//		return id;
//	}
//
//	public void setId(int id) {
//		this.id = id;
//	}
//
//	public String getEmail() {
//		return email;
//	}
//
//	public void setEmail(String email) {
//		this.email = email;
//	}
//
//	public String getGender() {
//		return gender;
//	}
//
//	public void setGender(String gender) {
//		this.gender = gender;
//	}
//
//	public String getAge() {
//		return age;
//	}
//
//	public void setAge(String age) {
//		this.age = age;
//	}
//
//	public String getCurrentWeight() {
//		return currentWeight;
//	}
//
//	public void setCurrentWeight(String currentWeight) {
//		this.currentWeight = currentWeight;
//	}
//
//	public String getGoalWeight() {
//		return goalWeight;
//	}
//
//	public void setGoalWeight(String goalWeight) {
//		this.goalWeight = goalWeight;
//	}
//
//	public String getDuration() {
//		return duration;
//	}
//
//	public void setDuration(String duration) {
//		this.duration = duration;
//	}
	
	
	
	
}
