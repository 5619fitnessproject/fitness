package edu.usyd.au.fitness.domain;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;


@Entity
@Table(name="USERACTIVITY")
@SequenceGenerator(name="seq_useractivity", initialValue=100001, allocationSize=100,sequenceName="seq_useractivity")
public class UserActivity implements Serializable {

	@Id
	@GeneratedValue
	@Column(name="Id")
	private int id;


	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name = "user_Id")//, unique = true)
	private User user;
	
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name = "activity_Id")//, unique = true)
	private Activity activity;
	
	@Temporal(TemporalType.DATE)
	@Column(name="CREATEDDATE")
    private Date createdDate;
	
	@Column(name="DURATION", length = 20)
    private double duration;
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Activity getActivity() {
		return activity;
	}

	public void setActivity(Activity activity) {
		this.activity = activity;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public double getDuration() {
		return duration;
	}

	public void setDuration(double duration) {
		this.duration = duration;
	}
}
