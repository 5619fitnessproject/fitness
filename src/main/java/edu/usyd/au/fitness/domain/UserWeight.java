package edu.usyd.au.fitness.domain;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;


@Entity
@Table(name="USERWEIGHT")
@SequenceGenerator(name="seq_userweight", initialValue=100001, allocationSize=100,sequenceName="seq_userweight")
public class UserWeight implements Serializable {

	@Id
	@GeneratedValue
	@Column(name="Id")
	private int id;

	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name = "user_Id")//, unique = true)
	private User user;
	
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name = "weight_Id")//, unique = true)
	private Weight weight;
	
	@Temporal(TemporalType.DATE)
	@Column(name="CREATEDDATE")
    private Date createdDate;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Weight getWeight() {
		return weight;
	}

	public void setWeight(Weight weight) {
		this.weight = weight;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
}