package edu.usyd.au.fitness.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name="FOOD")
@SequenceGenerator(name="seq_food", initialValue=100001, allocationSize=100,sequenceName="seq_food")
public class Food implements Serializable {

	@Id
	@GeneratedValue
	@Column(name="Id")
	private int id;

	//@Column(name="USERID")
	//private int user_id;
	
	@Column(name="FOODNAME", length = 20)
    private String foodName;
	
	@Column(name="AMOUNT", length = 20)
    private double amount;
	
	@Column(name="UNIT", length = 20)
    private String unit;
	
	@Column(name="CALS", length = 20)
    private double cals;
	
	@Column(name="FAT", length = 20)
    private double fat;
	
	@Column(name="CARBS", length = 20)
    private double carbs;
	
	@Column(name="PROT", length = 20)
    private double prot;
	
	@Column(name="CHOL", length = 20)
	private double chol;
	
	@Column(name="FIBER", length = 20)
	private double fiber;
	
	@Column(name="SUGAR", length = 20)
	private double sugar;
	
	@Column(name="CALCIUM", length = 20)
	private int calcium;
	
	@Column(name="IRON", length = 20)
	private int iron;
	
	@Column(name="SODIUM", length = 20)
	private double sodium;
	
	
	@Temporal(TemporalType.DATE)
	@Column(name="CREATEDDATE")
    private Date createdDate;
	
	@OneToMany(mappedBy="food",cascade=CascadeType.ALL)
	private Set<UserFood> userFoodList;
	
	public Set<UserFood> getUserFoodList() {
		return userFoodList;
	}

	public void setUserFoodList(Set<UserFood> userFoodList) {
		this.userFoodList = userFoodList;
	}

	public double getChol() {
		return chol;
	}

	public void setChol(double chol) {
		this.chol = chol;
	}

	public double getFiber() {
		return fiber;
	}

	public void setFiber(double fiber) {
		this.fiber = fiber;
	}

	public double getSugar() {
		return sugar;
	}

	public void setSugar(double sugar) {
		this.sugar = sugar;
	}

	public int getCalcium() {
		return calcium;
	}

	public void setCalcium(int calcium) {
		this.calcium = calcium;
	}

	public int getIron() {
		return iron;
	}

	public void setIron(int iron) {
		this.iron = iron;
	}

	public double getSodium() {
		return sodium;
	}

	public void setSodium(double sodium) {
		this.sodium = sodium;
	}

	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public double getCals() {
		return cals;
	}

	public void setCals(double cals) {
		this.cals = cals;
	}

	public double getFat() {
		return fat;
	}

	public void setFat(double fat) {
		this.fat = fat;
	}

	public double getCarbs() {
		return carbs;
	}

	public void setCarbs(double carbs) {
		this.carbs = carbs;
	}

	public double getProt() {
		return prot;
	}

	public void setProt(double prot) {
		this.prot = prot;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getFoodName() {
		return foodName;
	}

	public void setFoodName(String foodName) {
		this.foodName = foodName;
	}

	
}
