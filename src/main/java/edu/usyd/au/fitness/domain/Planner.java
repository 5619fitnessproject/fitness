package edu.usyd.au.fitness.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name="PLANNER")
@SequenceGenerator(name="seq_planner", initialValue=100001, allocationSize=100,sequenceName="seq_planner")
public class Planner implements Serializable {

	@Id
	@GeneratedValue
	@Column(name="Id")
	private int id;

	@Column(name="EMAIL", nullable = false, length = 40, unique = true)
	private String email;
	
	@Column(name="GENDER", nullable = false, length = 20)
    private String gender;
	
	@Column(name="HEIGHT", nullable = false, length = 20)
    private Double height;
	
	@Column(name="CURRENT_WEIGHT", nullable = false, length = 20)
    private Double currentWeight;
	
	@Column(name="GOAL_WEIGHT", nullable = false, length = 20)
    private Double goalWeight;
	
	@Column(name="DURATION", nullable = false, length = 20)
    private Double duration;
	
	@Column(name="STANDARD_WEIGHT", nullable = false, length = 20)
    private Double standardWeight;
	
	@Column(name="PERCENTAGE", nullable = false, length = 20)
    private Double percentage;
	
	@Column(name="CALORIE_PER_WEEK", nullable = false, length = 20)
    private Double caloriePerWeek;
	
	@OneToOne(mappedBy = "planner")
	private User user;
	
//	private Reminder reminder;
	

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Double getStandardWeight() {
		return standardWeight;
	}

	public void setStandardWeight(Double standardWeight) {
		this.standardWeight = standardWeight;
	}

	public Double getPercentage() {
		return percentage;
	}

	public void setPercentage(Double percentage) {
		this.percentage = percentage;
	}

	public Double getCaloriePerWeek() {
		return caloriePerWeek;
	}

	public void setCaloriePerWeek(Double caloriePerWeek) {
		this.caloriePerWeek = caloriePerWeek;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Double getHeight() {
		return height;
	}

	public void setHeight(Double height) {
		this.height = height;
	}

	public Double getCurrentWeight() {
		return currentWeight;
	}

	public void setCurrentWeight(Double currentWeight) {
		this.currentWeight = currentWeight;
	}

	public Double getGoalWeight() {
		return goalWeight;
	}

	public void setGoalWeight(Double goalWeight) {
		this.goalWeight = goalWeight;
	}

	public Double getDuration() {
		return duration;
	}

	public void setDuration(Double duration) {
		this.duration = duration;
	}
	
	
	
	
}
