package edu.usyd.au.fitness.domain;

public class FatSecretFood {
	private String food_name, serving_description,metric_serving_amount,
	metric_serving_unit,number_of_units,
	calories,carbohydrate,protein,fat,cholesterol,sodium,fiber,
	sugar,calcium,iron;
	
	public FatSecretFood(String food_name, String serving_description,String metric_serving_amount,
			String metric_serving_unit,String number_of_units,String calories,String carbohydrate,
			String protein,String fat,String cholesterol,String sodium,String fiber,String sugar,
			String calcium,String iron){
		this.food_name = food_name;
		this.serving_description = serving_description;
		this.metric_serving_amount = metric_serving_amount;
		this.metric_serving_unit = metric_serving_unit;
		this.number_of_units = number_of_units;
		this.calories = calories;
		this.carbohydrate = carbohydrate;
		this.protein = protein;
		this.fat = fat;
		this.cholesterol = cholesterol;
		this.sodium = sodium;
		this.fiber = fiber;
		this.sugar = sugar;
		this.calcium = calcium;
		this.iron = iron;
	}

	public String getFood_name() {
		return food_name;
	}

	public String getServing_description() {
		return serving_description;
	}

	public String getMetric_serving_amount() {
		return metric_serving_amount;
	}

	public String getMetric_serving_unit() {
		return metric_serving_unit;
	}

	public String getNumber_of_units() {
		return number_of_units;
	}

	public String getCalories() {
		return calories;
	}

	public String getCarbohydrate() {
		return carbohydrate;
	}

	public String getProtein() {
		return protein;
	}

	public String getFat() {
		return fat;
	}

	public String getCholesterol() {
		return cholesterol;
	}

	public String getSodium() {
		return sodium;
	}

	public String getFiber() {
		return fiber;
	}

	public String getSugar() {
		return sugar;
	}

	public String getCalcium() {
		return calcium;
	}

	public String getIron() {
		return iron;
	}
	
}


