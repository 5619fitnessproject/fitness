package edu.usyd.au.fitness.service;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import edu.usyd.au.fitness.domain.Activity;
import edu.usyd.au.fitness.domain.Food;
import edu.usyd.au.fitness.domain.Planner;
import edu.usyd.au.fitness.domain.User;
import edu.usyd.au.fitness.domain.UserActivity;
import edu.usyd.au.fitness.domain.UserFood;
import edu.usyd.au.fitness.domain.UserWeight;
import edu.usyd.au.fitness.domain.Weight;


@Service(value="fitnessService")
@Transactional
public class FitnessService {

	private SessionFactory sessionFactory;
	
	@Autowired
	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}
	
	public void addFood(Food food) {
		Session session = this.sessionFactory.getCurrentSession();
		food.setCreatedDate(new Date());
		session.save(food);
		session.flush();
		
	}
	
	public void addUserFood(UserFood userfood) {
		Session session = this.sessionFactory.getCurrentSession();
		userfood.setCreatedDate(new Date());
		session.save(userfood);
		session.flush();
		
	}
	public void addWeight(Weight weight) {
		Session session = this.sessionFactory.getCurrentSession();
		weight.setCreatedDate(new Date());
		session.save(weight);
		session.flush();
		
	}
	
	public void addUserWeight(UserWeight userweight) {
		Session session = this.sessionFactory.getCurrentSession();
		userweight.setCreatedDate(new Date());
		session.save(userweight);
		session.flush();
		
	}
	
	public void addUserActivity(UserActivity useractivity) {
		Session session = this.sessionFactory.getCurrentSession();
		useractivity.setCreatedDate(new Date());
		session.save(useractivity);
		session.flush();
		
	}
	
	public void updateUserActivity(UserActivity useractivity)
	{
		Session currentSession = this.sessionFactory.getCurrentSession();
		currentSession.merge(useractivity);
	}
	
	public void addActivity(Activity activity) {
		Session session = this.sessionFactory.getCurrentSession();
		activity.setCreatedDate(new Date());
		session.save(activity);
		session.flush();
		
	}
	
	public List<Food> queryFoods() {
		String sql = "from Food";
		sql += " order by id DESC";
		Query query = this.sessionFactory.getCurrentSession().createQuery(sql);
		
		List<Food> foods = query.list();
		
		return foods;
	}
	
	public List<Activity> queryActivities() {
		String sql = "from Activity";
		sql += " order by id DESC";
		Query query = this.sessionFactory.getCurrentSession().createQuery(sql);
		
		List<Activity> activities = query.list();
		
		return activities;
	}
	
	public List<Activity> queryActivitiesByName(String name) {
		
		String sql = "from Activity u";
		sql += " where u.name = '"+name+"'";
		Query query = this.sessionFactory.getCurrentSession().createQuery(sql);
		
		List<Activity> activities =  query.list();
		
		return activities;	
		
	}
	
	public List<User> queryUserByEmail(String email) {
		
		String sql = "from User u";
		sql += " where u.email = '"+email+"'";
		Query query = this.sessionFactory.getCurrentSession().createQuery(sql);
		
		List<User> users =  query.list();
		
		return users;	
		
	}
	
	public List<UserFood> queryUserFoodByUserId(int userid) {
		
		String sql = "from UserFood";
		sql += " where user_id = '"+userid+"'";
		Query query = this.sessionFactory.getCurrentSession().createQuery(sql);
		
		List<UserFood> userfood =  query.list();
		
		return userfood;	
		
	}
	
	public List<UserFood> queryUserFoodByUserIdandDate(int userid,String date) {
		
		String sql = "from UserFood";
		sql += " where user_id = '"+userid+"' and createdDate = '"+date+"'";
		Query query = this.sessionFactory.getCurrentSession().createQuery(sql);
		
		List<UserFood> userfood =  query.list();
		
		return userfood;	
		
	}
	
	public List<UserActivity> queryUserActivityByUserIdandDate(int userid,String date) {
		
		String sql = "from UserActivity";
		sql += " where user_id = '"+userid+"' and createdDate = '"+date+"'";
		Query query = this.sessionFactory.getCurrentSession().createQuery(sql);
		
		List<UserActivity> useractivity =  query.list();
		
		return useractivity;	
		
	}
	
	public User queryUserById(int id) {
		
		Session currentSession = this.sessionFactory.getCurrentSession();
		User user = (User) currentSession.get(User.class, id);		
		return user;			
	}
	
	public Food queryFoodById(int id) {
		
		Session currentSession = this.sessionFactory.getCurrentSession();
		Food food = (Food) currentSession.get(Food.class, id);		
		return food;		
	}
	
	public List<Food> queryFoodByfoodNmae(String foodname) {
		
		String sql = "from Food f";
		sql += " where f.foodName = '"+foodname+"'";
		Query query = this.sessionFactory.getCurrentSession().createQuery(sql);
		
		List<Food> foods =  query.list();
		
		return foods;	
		
	}
	
	public List<UserWeight> queryUserWeightByUserId(int userid) {
		
		String sql = "from UserWeight";
		sql += " where user_id = '"+userid+"'";
		Query query = this.sessionFactory.getCurrentSession().createQuery(sql);
		
		List<UserWeight> userweight =  query.list();
		
		return userweight;	
		
	}
	
	public Weight queryWeightById(int id) {
		
		Session currentSession = this.sessionFactory.getCurrentSession();
		Weight weight = (Weight) currentSession.get(Weight.class, id);		
		return weight;		
	}
	

}









