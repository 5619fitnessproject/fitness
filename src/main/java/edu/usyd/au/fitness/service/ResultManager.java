package edu.usyd.au.fitness.service;

import java.io.Serializable;
import java.util.List;

import edu.usyd.au.fitness.domain.Evaluation;
import edu.usyd.au.fitness.domain.Result;

public interface ResultManager extends Serializable{
    
    public List<Result> getResults(String userName);
    
    public void addResult(Result result);
    
    public Result getResultById(long id);
    
//    public void updateProduct(Evaluation evaluation);
//    
//    public void deleteProduct(long id);
    
}