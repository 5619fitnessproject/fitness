package edu.usyd.au.fitness.service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import edu.usyd.au.fitness.domain.Activity;
import edu.usyd.au.fitness.domain.Food;
import edu.usyd.au.fitness.domain.User;
import edu.usyd.au.fitness.domain.recommend.RecommendedActivity;
import edu.usyd.au.fitness.domain.recommend.RecommendedDiet;

import org.codehaus.jackson.JsonEncoding;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;


@Service(value="calendarService")
@Transactional
public class CalendarService {

	private SessionFactory sessionFactory;
	
	@Autowired
	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}
	
	public List<RecommendedActivity> queryRecommendedActivity() {
		String sql = "from RecommendedActivity";
		sql += " where enable != :code";
		sql += " order by SEQUENCY";
		Query query = this.sessionFactory.getCurrentSession().createQuery(sql);
		query.setParameter("code", false);
		List<RecommendedActivity> activities = query.list();
		
		return activities;
	}
	
	public String getJsonOfActivities(User u){
		User user = (User) this.sessionFactory.getCurrentSession().get(User.class, u != null?u.getId() : 1);
		
		OutputStream out = new ByteArrayOutputStream();
		JsonFactory jfactory = new JsonFactory();
		Date startDate;
		if(user.getCreatedDate() !=null){
			startDate = user.getCreatedDate();
		}
		else{
			startDate = new Date();
		}
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		
	    JsonGenerator jGenerator;
		try {
			jGenerator = jfactory.createJsonGenerator(out, JsonEncoding.UTF8);
			ObjectMapper mapper = new ObjectMapper();
		    jGenerator.writeStartArray(); 
		    List<RecommendedActivity> activities= queryRecommendedActivity();
		    
		    for (RecommendedActivity activity : activities) {
		    	Date sdate = new Date();
		    	sdate.setTime(startDate.getTime() + (activity.getSequency()) * 24 * 60 * 60 * 1000);
		    	Date edate = new Date();
		    	edate.setTime(sdate.getTime() + (activity.getRangeOfDate() - 1) * 24 * 60 * 60 * 1000);
		    	
		    	activity.setStart(formatter.format(sdate));
		    	activity.setEnd(formatter.format(edate));
		    }
		    
		    for (RecommendedActivity activity : activities) {
		    	RecommendedDiet diet = new RecommendedDiet();
		    	diet.setTitle("Diet");
		    	diet.setBreakfast("1 cup high fiber cereal.  1/2 cup milk/soy milk.  1 banana");
		    	diet.setLunch("1 cup pasta. 2 teaspoons of olive oil. 2 ounces of lean meat");
		    	diet.setSnack("1 cup raw carrot sticks/celery/green peppers");
		    	diet.setDinner("6 crackers. 1 cup low-fat cottage-cheese. 1/2 ounce mixed nuts");
		    	diet.setStart(activity.getStart());
		    	diet.setEnd(activity.getEnd());
		    	
		        String usage = mapper.writeValueAsString(diet);
		        jGenerator.writeRaw(usage + ",");                                                                                               
		        // here, big hassles to write a comma to separate json objects, when the last object in the list is reached, no comma 
		    }
		    
		    for (RecommendedActivity activity : activities) {
		        String usage = mapper.writeValueAsString(activity);
		        jGenerator.writeRaw(usage + ",");                                                                                               
		        // here, big hassles to write a comma to separate json objects, when the last object in the list is reached, no comma 
		    }
		    
		    jGenerator.writeEndArray(); // ]

		    jGenerator.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	    
	    System.out.println(out.toString());
	    
	    return out.toString();
	}
	
	public void addAct(RecommendedActivity act) {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(act);
		session.flush();
	}
	
	public void updateAct(RecommendedActivity act) {
		Session session = this.sessionFactory.getCurrentSession();
		session.merge(act);
		session.flush();
	}
	
	public void addFood(Food food) {
		Session session = this.sessionFactory.getCurrentSession();
		food.setCreatedDate(new Date());
		session.save(food);
		session.flush();
		
	}
	
	public void addActivity(Activity activity) {
		Session session = this.sessionFactory.getCurrentSession();
		activity.setCreatedDate(new Date());
		session.save(activity);
		session.flush();
		
	}
	
	public List<Food> queryFoods() {
		String sql = "from Food";
		sql += " order by id DESC";
		Query query = this.sessionFactory.getCurrentSession().createQuery(sql);
		
		List<Food> foods = query.list();
		
		return foods;
	}
	
	public List<Food> queryActivities() {
		String sql = "from Activity";
		sql += " order by id DESC";
		Query query = this.sessionFactory.getCurrentSession().createQuery(sql);
		
		List<Food> activities = query.list();
		
		return activities;
	}
	
}









