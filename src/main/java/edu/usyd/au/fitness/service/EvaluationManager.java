package edu.usyd.au.fitness.service;

import java.io.Serializable;
import java.util.List;

import edu.usyd.au.fitness.domain.Evaluation;

public interface EvaluationManager extends Serializable{

    
    public List<Evaluation> getEvaluations();
    
    public void addEvaluation(Evaluation evaluation);
    
    public Evaluation getEvaluationById(long id);
  
    
}