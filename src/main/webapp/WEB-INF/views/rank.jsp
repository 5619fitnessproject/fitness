<%@ include file="/WEB-INF/views/include.jsp"%>
<html>
<head>
	<%@ include file="/WEB-INF/views/header.jsp"%>
	<script type="text/javascript" src="<c:url value='/resources/js/jquery.validate.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/resources/js/validator.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/resources/js/enquire.js'/>"></script>
</head>
<body>
<div class="main">
  <div class="main_resize">
    <div class="header">
    	<%@ include file="/WEB-INF/views/head.jsp"%>
    </div>
    <div class="content">
      <div class="content_bg">
        <div class="mainbar">
          <div class="article">
            <div class="clr"></div>
          </div>
          <div class="article">
           <label for="rank">Hi, <c:out value="${currentUser.userName}"/>, Your rank is <c:out value="${currentUser.rank}"/>!!!</label>
          <table id="mytable" cellspacing="0">
            	<tr>
            		<th>RANK</th>
            		<th>USER</th>
            		<th>GOLD</th>
            		<th>SILVER</th>
            		<th>BRONZE</th>
            		<th>CREDITS</th>
            	</tr>
            	<c:forEach items="${model.awards}" var="award">
				<tr>
            		<td class="">${award.rank}</td>
            		<td class="">${award.userName}</td>
            		<td class="">${award.gold}</td>
            		<td class="">${award.silver}</td>
            		<td class="">${award.bronze}</td>
            		<td class="">${award.credit}</td>
            	</tr>
				</c:forEach>
            </table>
          </div>
          <div class="pagenavi"><span class="pages"> </span></div>
        </div>
        <%@ include file="/WEB-INF/views/menu.jsp"%>
        <script type="text/javascript">$("#RANK").addClass("active")</script>
        <div class="clr"></div>
      </div>
    </div>
  </div>
  <div class="fbg">
    <div class="fbg_resize">
      <div class="clr"></div>
    </div>
  </div>
</div>
<div class="footer">
  <div class="footer_resize">
    <p class="lf">&copy;  University of Sydney - School of Information technology.</p>
    <div class="clr"></div>
  </div>
</div>
</html>
