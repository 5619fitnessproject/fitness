<%@ include file="/WEB-INF/views/include.jsp"%>
<html>
<head>
	<%@ include file="/WEB-INF/views/header.jsp"%>
	<script type="text/javascript" src="<c:url value='/resources/js/jquery.validate.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/resources/js/validator.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/resources/js/enquire.js'/>"></script>
</head>
<body>
<div class="main">
  <div class="main_resize">
    <div class="header">
    	<%@ include file="/WEB-INF/views/head.jsp"%>
    </div>
    <div class="content">
      <div class="content_bg">
        <div class="mainbar">
          <div class="article">
            <div class="clr"></div>
            <c:if test="${model.isAddedSuc}">
          		<class="post-data">A record had been added successfully.</p>
            </c:if>
            <div class="clr"></div>
          </div>
          <div class="article">
          <form:form method="POST" action="activitySearch" id="activityForm">
          <label>Activity name:</label>
          <input type="text" name="activityName"/>
          <input type="submit" value="Search"/><br>
          <c:out value="${model.nofind}" />                
          </form:form>
          <c:if test="${model.isSelect}">
     	  <div>
     	  <table>
		  <form:form modelAttribute="activity" method="POST" action="saveActivity" id="activityForm">	
		  <tr><td>	  
		  <label>Activity name:</label>
     	  <input id="name" name="name" type="text" value="${model.activitiesinf.name}" readonly="readonly"/>
     	  </tr></td>
     	  <tr><td>
     	  <label>Duration(hours):</label>
     	  <input id="duration" name="duration" type="text" value="${model.activitiesinf.duration}" />  
     	  </tr></td>
     	  <tr><td>  	  
     	  <label>Calories Burned:</label>
     	  <input id="cals" name="cals" type="text" value="${model.activitiesinf.cals}" readonly="readonly"/>
     	  </tr></td>
     	  <tr><td>
		  <input type="submit" value="Add"/>
		  </tr></td>
     	  </form:form>
     	  </table>
     	  </div>
     	  </c:if>
     	  
     	  <div>
          <table id="mytable" cellspacing="0">
          <label>Your Activity Log:</label>
		  <tr>
            <th>Activity Name</th>
            <th>Duration(minutes)</th>
            <th>Calories Burned</th>
          </tr>
          <c:set value="0" var="sum" />
          <c:forEach items="${model.useractivityInfo}" var="activities" varStatus="status">
				<tr>
            		<td class="">${activities.activity.name}</td>
            		<td class="">${activities.duration}</td>
            		<td class="">${activities.duration*activities.activity.cals}</td>     
					<c:set value="${sum + activities.duration*activities.activity.cals}" var="sum" />                		
            	</tr>
          </c:forEach>
          <tr>
          <td></td>
          <td></td>
          <td>Total: ${sum} </td>
          </tr>
          </table>
       </div>
     	  
<%--             <form:form modelAttribute="activity" method="POST" action="saveActivity" id="activityForm"> --%>
<!--               <ol> -->
<!--               	<li> -->
<!--                   <label for="name">ACTIVITY NAME<font class="red">*</font></label> -->
<!--                   <input id="name" name="name" type="text" class="text" /> -->
<!--                 </li> -->
<!--                 <li> -->
<!--                   <label for="details">DETAILS <font class="red">*</font></label> -->
<!--                   <input id="details" name="details" type="text" class="text" /> -->
<!--                 </li> -->
<!--                 <li> -->
<!--                   <label for="unit">UNIT <font class="red">*</font></label> -->
<!--                   <input id="unit" name="unit" type="text" class="text" /> -->
<!--                 </li> -->
<!--                 <li> -->
<!--                   <label for="cals">CALS <font class="red">*</font></label> -->
<!--                   <input id="cals" name="cals" type="text" class="text" /> -->
<!--                 </li> -->
<!--                 <li> -->
<%--                   <input value="Submmit" type="submit" name="imageField" id="imageField" src="${pageContext.request.contextPath}/resources/images/submit.gif" class="send" /> --%>
<!--                   <div class="clr"></div> -->
<!--                 </li> -->
<!--               </ol> -->
<%--             </form:form> --%>
          </div>
          <div class="pagenavi"><span class="pages"> </span></div>
        </div>
        <%@ include file="/WEB-INF/views/menu.jsp"%>
        <script type="text/javascript">$("#ACTIVITY").addClass("active")</script>
        <div class="clr"></div>
      </div>
    </div>
  </div>
  <div class="fbg">
    <div class="fbg_resize">
      <div class="clr"></div>
    </div>
  </div>
</div>
<div class="footer">
  <div class="footer_resize">
    <p class="lf">&copy;  University of Sydney - School of Information technology.</p>
    <div class="clr"></div>
  </div>
</div>
</html>
