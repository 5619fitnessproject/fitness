<div class="sidebar">
  <div class="gadget">
  	<h4 class="star"><span>ACCOUNT</span> </h4>
    <div class="clr"></div>
    <ul class="sb_menu">
      <li id="SIGNOUT"><a href="${pageContext.request.contextPath}/">SIGN OUT</a></li>
      <li id="UPDATEACCOUNT"><a href="${pageContext.request.contextPath}/update">UPDATE</a></li>
    </ul>
  </div>
  <div class="gadget">
  	<h4 class="star"><span>HOME</span> </h4>
    <div class="clr"></div>
    <ul class="sb_menu">
      <li id="CALENDAR"><a href="${pageContext.request.contextPath}/calendar">CALENDAR</a></li>
<%--       <li id="PROFILE"><a href="${pageContext.request.contextPath}">PROFILE</a></li> --%>
    </ul>
  </div>  
  <div class="gadget">
  	<h4 class="star"><span>LOG</span> </h4>
    <div class="clr"></div>
    <ul class="sb_menu">
      <li id="WEIGHT"><a href="${pageContext.request.contextPath}/weight">WEIGHT</a></li>
      <li id="FOOD"><a href="${pageContext.request.contextPath}/food">FOOD</a></li>
      <li id="ACTIVITY"><a href="${pageContext.request.contextPath}/activity">ACTIVITY</a></li>
      <!-- <li><a href="#">WEIGHT</a></li> -->
    </ul>
  </div>
  
<!-- ////////////Eric's part///////////////////////////   -->
  
  <div class="gadget">
  	<h4 class="star"><span>PLANNER & REMINDER</span> </h4>
    <div class="clr"></div>
    <ul class="sb_menu">
<%--       <li id="REGISTER"><a href="${pageContext.request.contextPath}/register">REGISTER</a></li> --%>
      <li id="PLANNER"><a href="${pageContext.request.contextPath}/planner">PLANNER</a></li> 
      <li id="REMINDER"><a href="${pageContext.request.contextPath}/reminder">REMINDER</a></li>
    </ul>
  </div>
   <!-- ////////////Hill's part///////////////////////////   -->
  
  <div class="gadget">
  	<h4 class="star"><span>Evaluation</span> </h4>
    <div class="clr"></div>
    <ul class="sb_menu">
      <li id="EVALUATION"><a href="${pageContext.request.contextPath}/evaluation">Daily Evaluation</a></li>
      <li id="HISTORY"><a href="${pageContext.request.contextPath}/history">History</a></li>
      <li id="RANK"><a href="${pageContext.request.contextPath}/rank">Rank</a></li>
    </ul>
  </div>
  
<!-- ////////////Hill's part///////////////////////////   -->  
  
<!-- ////////////Eric's part///////////////////////////   -->  

  <div class="gadget">
  	<h4 class="star"><span>CHARTS & TRACKERS</span> </h4>
    <div class="clr"></div>
    <ul class="sb_menu">
      <li id="CHARTS"><a href="${pageContext.request.contextPath}/charts">CHARTS & TRACKERS</a></li>
    </ul>
  </div>
  
  <div class="gadget">
  	<h4 class="star"><span>STATISTIC</span> </h4>
    <div class="clr"></div>
    <ul class="sb_menu">
      <li id="FOODRECORDS"><a href="${pageContext.request.contextPath}/foodRecords">FOOD RECORDS</a></li>
      <li id="ACTIVITYRECORDS"><a href="${pageContext.request.contextPath}/activityRecords">ACTIVITY RECORDS</a></li>
      <!-- <li><a href="#">WEIGHT</a></li> -->
    </ul>
  </div>
</div>