<%@ include file="/WEB-INF/views/include.jsp"%>
<%@ page import="edu.usyd.au.fitness.service.*" %> 
<html>
<head>
	<%@ include file="/WEB-INF/views/header.jsp"%>
	<script type="text/javascript" src="<c:url value='/resources/js/jquery.validate.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/resources/js/validator.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/resources/js/enquire.js'/>"></script>
	<script type="text/javascript" src="//www.google.com/jsapi"></script>
    <script type="text/javascript">
      google.load('visualization', '1.1', {packages: ['corechart', 'controls']});
    </script>
    <script type="text/javascript">
      function weightChart(para) {
        var dashboard = new google.visualization.Dashboard(
             document.getElementById('dashboard'));
      
         var control = new google.visualization.ControlWrapper({
           'controlType': 'ChartRangeFilter',
           'containerId': 'control',
           'options': {
             // Filter by the date axis.
             'filterColumnIndex': 0,
             'ui': {
               'chartType': 'LineChart',
               'chartOptions': {
                 'chartArea': {'width': '90%'},
                 'hAxis': {'baselineColor': 'none'}
               },
               // Display a single series that shows the closing value of the stock.
               // Thus, this view has two columns: the date (axis) and the stock value (line series).
               'chartView': {
                 'columns': [0, 1]
               },
               // 1 day in milliseconds = 24 * 60 * 60 * 1000 = 86,400,000
               'minRangeSize': 86400000
             }
           },
           // Initial range: 2012-02-09 to 2012-03-20.
           'state': {'range': {'start': new Date(2013, 9, 25), 'end': new Date(2013, 9, 30)}}
         });
      if(para==1)
    	  {
         var chart = new google.visualization.ChartWrapper({
		   'chartType': 'LineChart',
           'containerId': 'chart',
           'options': {
             // Use the same chart area width as the control for axis alignment.
             'chartArea': {'height': '80%', 'width': '90%'},
             'hAxis': {'slantedText': false},
             'vAxis': {'viewWindow': {'min': 60, 'max': 100}},
             'legend': {'position': 'none'}
           },
           // Convert the first column from 'date' to 'string'.
           'view': {
             'columns': [
               {
                 'calc': function(dataTable, rowIndex) {
                   return dataTable.getFormattedValue(rowIndex, 0);
                 },
                 'type': 'string'
               }, 1, 2]
           }
         });
         var data = new google.visualization.DataTable();
         data.addColumn('date', 'Date');
         data.addColumn('number', 'Current Weight');
         data.addColumn('number', 'Goal Weight');
// 		 data.addRow(['Year', 'Current Weight', 'Goal Weight']);
         var weight=new Array();
         var day=new Array();
         var goalweight=parseInt("${model.planner.goalWeight}");
         <c:forEach items="${model.weight}" var="weight" varStatus="status">
         weight["${status.index}"]=parseInt("${weight.currentWeight}");
         day["${status.index}"]=parseInt("${status.index}");
             data.addRow([new Date(2013,9,22+day["${status.index}"]),weight["${status.index}"],goalweight]);           
         </c:forEach>
    	  }
      if(para==2)
	  {
	     var chart = new google.visualization.ChartWrapper({
		   'chartType': 'ColumnChart',
	       'containerId': 'chart',
	       'options': {
	         // Use the same chart area width as the control for axis alignment.
	         'chartArea': {'height': '80%', 'width': '90%'},
	         'hAxis': {'slantedText': false},
	         'vAxis': {'viewWindow': {'min': 0, 'max': 1000}},
	         'legend': {'position': 'none'}
	       },
	       // Convert the first column from 'date' to 'string'.
	       'view': {
	         'columns': [
	           {
	             'calc': function(dataTable, rowIndex) {
	               return dataTable.getFormattedValue(rowIndex, 0);
	             },
	             'type': 'string'
	           }, 1]
	       }
	     });
	     <c:set value="0" var="sum25" />
         <c:forEach items="${model.userfoodInfo25}" var="foods" varStatus="status">
         <c:set value="${sum25 + foods.amount*foods.food.cals}" var="sum25" />         
         </c:forEach>
         <c:set value="0" var="sum26" />
         <c:forEach items="${model.userfoodInfo26}" var="foods" varStatus="status">
         <c:set value="${sum26 + foods.amount*foods.food.cals}" var="sum26" />         
         </c:forEach>
         <c:set value="0" var="sum27" />
         <c:forEach items="${model.userfoodInfo27}" var="foods" varStatus="status">
         <c:set value="${sum27 + foods.amount*foods.food.cals}" var="sum27" />         
         </c:forEach>
         <c:set value="0" var="sum28" />
         <c:forEach items="${model.userfoodInfo28}" var="foods" varStatus="status">
         <c:set value="${sum28 + foods.amount*foods.food.cals}" var="sum28" />         
         </c:forEach>
         <c:set value="0" var="sum29" />
         <c:forEach items="${model.userfoodInfo29}" var="foods" varStatus="status">
         <c:set value="${sum29 + foods.amount*foods.food.cals}" var="sum29" />         
         </c:forEach>
         <c:set value="0" var="sum30" />
         <c:forEach items="${model.userfoodInfo30}" var="foods" varStatus="status">
         <c:set value="${sum30 + foods.amount*foods.food.cals}" var="sum30" />         
         </c:forEach>
	     var data = new google.visualization.DataTable();
         data.addColumn('date', 'Date');
         data.addColumn('number', 'Calories from Food');
         //data.addColumn('number', 'Goal Weight');
// 		 data.addRow(['Year', 'Current Weight', 'Goal Weight']);
         var calories25=parseInt("${sum25}");
         var calories26=parseInt("${sum26}");
         var calories27=parseInt("${sum27}");
         var calories28=parseInt("${sum28}");
         var calories29=parseInt("${sum29}");
         var calories30=parseInt("${sum30}");
         data.addRow([new Date(2013,9,25),calories25]);
         data.addRow([new Date(2013,9,26),calories26]); 
         data.addRow([new Date(2013,9,27),calories27]);
         data.addRow([new Date(2013,9,28),calories28]);
         data.addRow([new Date(2013,9,29),calories29]);
         data.addRow([new Date(2013,9,30),calories30]);         
     
	  }
      if(para==3)
	  {
	     var chart = new google.visualization.ChartWrapper({
		   'chartType': 'ColumnChart',
	       'containerId': 'chart',
	       'options': {
	         // Use the same chart area width as the control for axis alignment.
	         'chartArea': {'height': '80%', 'width': '90%'},
	         'hAxis': {'slantedText': false},
	         'vAxis': {'viewWindow': {'min': 0, 'max': 1000}},
	         'legend': {'position': 'none'}
	       },
	       // Convert the first column from 'date' to 'string'.
	       'view': {
	         'columns': [
	           {
	             'calc': function(dataTable, rowIndex) {
	               return dataTable.getFormattedValue(rowIndex, 0);
	             },
	             'type': 'string'
	           }, 1]
	       }
	     });
	     <c:set value="0" var="sum25" />
         <c:forEach items="${model.useractivityInfo25}" var="activities" varStatus="status">
         <c:set value="${sum25 + activities.duration*activities.activity.cals}" var="sum25" />         
         </c:forEach>
         <c:set value="0" var="sum26" />
         <c:forEach items="${model.useractivityInfo26}" var="activities" varStatus="status">
         <c:set value="${sum26 + activities.duration*activities.activity.cals}" var="sum26" />         
         </c:forEach>
         <c:set value="0" var="sum27" />
         <c:forEach items="${model.useractivityInfo27}" var="activities" varStatus="status">
         <c:set value="${sum27 + activities.duration*activities.activity.cals}" var="sum27" />         
         </c:forEach>
         <c:set value="0" var="sum28" />
         <c:forEach items="${model.useractivityInfo28}" var="activities" varStatus="status">
         <c:set value="${sum28 + activities.duration*activities.activity.cals}" var="sum28" />         
         </c:forEach>
         <c:set value="0" var="sum29" />
         <c:forEach items="${model.useractivityInfo29}" var="activities" varStatus="status">
         <c:set value="${sum29 + activities.duration*activities.activity.cals}" var="sum29" />         
         </c:forEach>
         <c:set value="0" var="sum30" />
         <c:forEach items="${model.useractivityInfo30}" var="activities" varStatus="status">
         <c:set value="${sum30 + activities.duration*activities.activity.cals}" var="sum30" />         
         </c:forEach>
	     var data = new google.visualization.DataTable();
         data.addColumn('date', 'Date');
         data.addColumn('number', 'Calories from Fitness');
         //data.addColumn('number', 'Goal Weight');
// 		 data.addRow(['Year', 'Current Weight', 'Goal Weight']);
         var calories25=parseInt("${sum25}");
         var calories26=parseInt("${sum26}");
         var calories27=parseInt("${sum27}");
         var calories28=parseInt("${sum28}");
         var calories29=parseInt("${sum29}");
         var calories30=parseInt("${sum30}");
         data.addRow([new Date(2013,9,25),calories25]);
         data.addRow([new Date(2013,9,26),calories26]); 
         data.addRow([new Date(2013,9,27),calories27]);
         data.addRow([new Date(2013,9,28),calories28]);
         data.addRow([new Date(2013,9,29),calories29]);
         data.addRow([new Date(2013,9,30),calories30]);
         
     
	  }   
       if(para==4)
	  {
	     var chart = new google.visualization.ChartWrapper({
		   'chartType': 'ColumnChart',
	       'containerId': 'chart',
	       'options': {
	         // Use the same chart area width as the control for axis alignment.
	         'chartArea': {'height': '80%', 'width': '90%'},
	         'hAxis': {'slantedText': false},
	         'vAxis': {'viewWindow': {'min': 0, 'max': 200}},
	         'legend': {'position': 'none'}
	       },
	       // Convert the first column from 'date' to 'string'.
	       'view': {
	         'columns': [
	           {
	             'calc': function(dataTable, rowIndex) {
	               return dataTable.getFormattedValue(rowIndex, 0);
	             },
	             'type': 'string'
	           }, 1]
	       }
	     });
	     <c:set value="0" var="sum25" />
         <c:forEach items="${model.userfoodInfo25}" var="foods" varStatus="status">
         <c:set value="${sum25 + foods.amount*foods.food.carbs}" var="sum25" />         
         </c:forEach>
         <c:set value="0" var="sum26" />
         <c:forEach items="${model.userfoodInfo26}" var="foods" varStatus="status">
         <c:set value="${sum26 + foods.amount*foods.food.carbs}" var="sum26" />         
         </c:forEach>
         <c:set value="0" var="sum27" />
         <c:forEach items="${model.userfoodInfo27}" var="foods" varStatus="status">
         <c:set value="${sum27 + foods.amount*foods.food.carbs}" var="sum27" />         
         </c:forEach>
         <c:set value="0" var="sum28" />
         <c:forEach items="${model.userfoodInfo28}" var="foods" varStatus="status">
         <c:set value="${sum28 + foods.amount*foods.food.carbs}" var="sum28" />         
         </c:forEach>
         <c:set value="0" var="sum29" />
         <c:forEach items="${model.userfoodInfo29}" var="foods" varStatus="status">
         <c:set value="${sum29 + foods.amount*foods.food.carbs}" var="sum29" />         
         </c:forEach>
         <c:set value="0" var="sum30" />
         <c:forEach items="${model.userfoodInfo30}" var="foods" varStatus="status">
         <c:set value="${sum30 + foods.amount*foods.food.carbs}" var="sum30" />         
         </c:forEach>
	     var data = new google.visualization.DataTable();
         data.addColumn('date', 'Date');
         data.addColumn('number', 'CARBOHYDRATE');
         //data.addColumn('number', 'Goal Weight');
// 		 data.addRow(['Year', 'Current Weight', 'Goal Weight']);
         var calories25=parseInt("${sum25}");
         var calories26=parseInt("${sum26}");
         var calories27=parseInt("${sum27}");
         var calories28=parseInt("${sum28}");
         var calories29=parseInt("${sum29}");
         var calories30=parseInt("${sum30}");
         data.addRow([new Date(2013,9,25),calories25]);
         data.addRow([new Date(2013,9,26),calories26]); 
         data.addRow([new Date(2013,9,27),calories27]);
         data.addRow([new Date(2013,9,28),calories28]);
         data.addRow([new Date(2013,9,29),calories29]);
         data.addRow([new Date(2013,9,30),calories30]);         
     
	  } 
      
         dashboard.bind(control, chart);
         dashboard.draw(data);
      }
     

      //google.setOnLoadCallback(drawVisualization);
    </script>

</head>
<body>
<div class="main">
  <div class="main_resize">
    <div class="header">
    	<%@ include file="/WEB-INF/views/head.jsp"%>
    </div>
    <div class="content">
      <div class="content_bg">
        <div class="mainbar">
          <div class="article">
            <div class="clr"></div>
            <c:if test="${isAddedSuc}">
          		<class="post-data">A record had been added successfully.</p>
            </c:if>
            <div class="clr"></div>
          </div>
          <div class="article" >
    
           <input type="button" value="WEIGHT" onclick="weightChart(1)"/>
           <input type="button" value="CALORIES FROM FOOD" onclick="weightChart(2)"/> 
           <input type="button" value="CALORIES FROM FITNESS" onclick="weightChart(3)"/>
           <input type="button" value="CARBOHYDRATE" onclick="weightChart(4)"/>
   
<!--            CHOLESTEROL -->
<!--            CARBOHYDRATE -->
<!--            IRON -->
          
           <div id="dashboard" style="width: 620px; height: 480px;">
        	<div id="chart" style='width: 620px; height: 300px;'></div>
        	<div id="control" style='width: 620px; height: 50px;'></div>
   			 </div>
          </div>
          <div class="pagenavi"><span class="pages"> </span></div>
        </div>
        <%@ include file="/WEB-INF/views/menu.jsp"%>
        <script type="text/javascript">$("#CHARTS").addClass("active")</script>
        <div class="clr"></div>
      </div>
    </div>
  </div>
  <div class="fbg">
    <div class="fbg_resize">
      <div class="clr"></div>
    </div>
  </div>
</div>
<div class="footer">
  <div class="footer_resize">
    <p class="lf">&copy;  University of Sydney - School of Information technology.</p>
    <div class="clr"></div>
  </div>
</div>
</html>
