<%@ include file="/WEB-INF/views/include.jsp"%>
<html>
<head>
	<%@ include file="/WEB-INF/views/header.jsp"%>
	<script type="text/javascript" src="<c:url value='/resources/js/jquery.validate.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/resources/js/validator.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/resources/js/enquire.js'/>"></script>
</head>
<body>
<div class="main">
  <div class="main_resize">
    <div class="header">
    	<%@ include file="/WEB-INF/views/head.jsp"%>
    </div>
    <div class="content">
      <div class="content_bg">
       <% Object user = session.getAttribute("user");%>
        <div class="mainbar">
          <div class="article">
          <h3>Your  planner information: </h3>
              <ol>
              	<li>
              	  <label for="email">Login ID (Email): <font color="blue"><c:out value="${user.email}" /></font></label>
                </li>
                <li>
                  <label for="gender">Gender: <font color="blue"><c:out value="${model.planner.gender}" /></font></label>    
                </li>
                <li>
                  <label for="height">Height (cm): <font color="blue"><c:out value="${model.planner.height}" /></font></label>           
                </li>
                <li>
                  <label for="currentWeight">Current Weight (Kg.): <font color="blue"><c:out value="${model.planner.currentWeight}" /></font></label>           
                </li>
                <li>
                  <label for="goalWeight">Goal Weight (Kg.): <font color="blue"><c:out value="${model.planner.goalWeight}" /></font></label>           
                </li>
                <li>
                  <label for="duration">Duration: <font color="blue"><c:out value="${model.planner.duration}" /></font></label>           
                </li>

                <li>
                <form:form modelAttribute="editPlanner" method="POST" action="editPlanner" id="editForm">
<%--                   <input type="hidden" name="email" value="${model.planner.email}"/> --%>
                  <input type="hidden" name="gender" value="${model.planner.gender}"/>
                  <input type="hidden" name="height" value="${model.planner.height}"/>
                  <input type="hidden" name="currentWeight" value="${model.planner.currentWeight}"/>
                  <input type="hidden" name="goalWeight" value="${model.planner.goalWeight}"/>
                  <input type="hidden" name="duration" value="${model.planner.duration}"/>
                  <input value="   Edit    " type="submit" name="imageField" id="imageField" src="${pageContext.request.contextPath}/resources/images/submit.gif" class="send" />
                  <div class="clr"></div>
                </form:form>
                  
                <form:form method="POST" action="savePlanner" id="submitForm">
                  <input type="hidden" name="email" value="${user.email}"/>
                  <input type="hidden" name="gender" value="${model.planner.gender}"/>
                  <input type="hidden" name="height" value="${model.planner.height}"/>
                  <input type="hidden" name="currentWeight" value="${model.planner.currentWeight}"/>
                  <input type="hidden" name="goalWeight" value="${model.planner.goalWeight}"/>
                  <input type="hidden" name="duration" value="${model.planner.duration}"/>
                  <input value="Submmit" type="submit" name="imageField" id="imageField" src="${pageContext.request.contextPath}/resources/images/submit.gif" class="send" />
                  <div class="clr"></div>
                </form:form>
                </li>
              </ol>
          </div>
          <div class="pagenavi"><span class="pages"> </span></div>
        </div>
        <%@ include file="/WEB-INF/views/menu.jsp"%>
        <script type="text/javascript">$("#PLANNER").addClass("active")</script>
        <div class="clr"></div>
      </div>
    </div>
  </div>
  <div class="fbg">
    <div class="fbg_resize">
      <div class="clr"></div>
    </div>
  </div>
</div>
<div class="footer">
  <div class="footer_resize">
    <p class="lf">&copy;  University of Sydney - School of Information technology.</p>
    <div class="clr"></div>
  </div>
</div>
</html>
