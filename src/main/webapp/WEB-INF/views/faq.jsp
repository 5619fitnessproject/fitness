<%@ include file="/WEB-INF/views/include.jsp"%>
<html>
<head>
	<title>Enquiry</title>
	<%@ include file="/WEB-INF/views/header.jsp"%>
</head>
<body>
<div class="main">
  <div class="main_resize">
    <div class="header">
      <div class="logo">
        <h1><a href="#"><span>Online</span>Enquiry<small>School of Information Technology</small></a></h1>
      </div>
      <div class="search">
        <a href="http://sydney.edu.au"><img alt="" src="http://sydney.edu.au//images/common/university_sydney_logo.gif"></a>
        <!--/searchform -->
        <div class="clr"></div>
      </div>
      <div class="clr"></div>
      <div class="hbg"><img src="http://sydney.edu.au/engineering/it/images/frontpage/flashpod/1.jpg" width="923" height="155" alt="" /></div>
    </div>
    <div class="content">
      <div class="content_bg">
        <div class="mainbar">
          <div class="article">
            <h4><span>Enrolment</span></h4>
            <div class="clr"></div>
            <h6><a class="faq" href="#">Can I enrol online?</a></h6>
            <h6><a class="faq" href="#">When can I change my units of study?</a></h6>
            <h6><a class="faq" href="#">How do I change my units of study?</a></h6>
            <h6><a class="faq" href="#">What is Special Permission and how do I get it?</a></h6>
            <h6><a class="faq" href="#">How do I defer/suspend/discontinue my degree?</a></h6>
            <h6><a class="faq" href="#">How do I re-enrol after a suspension?</a></h6>
            <h6><a class="faq" href="#">How and when do I apply to transfer to a different IT degree?</a></h6>
            <h6><a class="faq" href="#">Where do I find credit for prior tertiary study information?</a></h6>
            <h6><a class="faq" href="#">Where is the Faculty of Engineering & IT Office and what are its opening hours?</a></h6>
            <h6><a class="faq" href="#">Can I study part-time?</a></h6>
			<h4><span>Studying</span></h4>
            <div class="clr"></div>
            <h6><a class="faq" href="#">Where can I find my timetable?</a></h6>
            <h6><a class="faq" href="#">Can I change class times on my timetable?</a></h6>
            <h6><a class="faq" href="#">When are my exams?  What are the semester dates?</a></h6>
            <h6><a class="faq" href="#">Where can I find information about textbooks?</a></h6>
            <h6><a class="faq" href="#">I need to apply for special consideration for an assessment/exam, how do I do it?</a></h6>
            <h6><a class="faq" href="#">Where can I find assignment cover sheets?</a></h6>
            <h6><a class="faq" href="#">I am worried about my exams, what do I need to do? </a></h6>
            <h4><span>Policies</span></h4>
            <div class="clr"></div>
            <h6><a class="faq" href="#">Where can I find out about academic honesty, plagiarism, how to reference etc.?</a></h6>
            <h4><span>Others</span></h4>
            <div class="clr"></div>
            <h6><a class="faq" href="#">I have a problem and need some advice. Who do I see?</a></h6>
            <h6><a class="faq" href="#">When is my graduation ceremony?</a></h6>
            <h6><a class="faq" href="#">Where can I provide feedback?</a></h6>
            <h6><a class="faq" href="#">How do I contact members of staff?</a></h6>
            <h6><a class="faq" href="#">How can I find out about scholarships?</a></h6>
            <h6><a class="faq" href="#">Where can I find information not in this FAQ?</a></h6>
            <div class="clr"></div>
          </div>
          <div class="pagenavi"><span class="pages"> </span></div>
        </div>
        <div class="sidebar">
          <div class="gadget">
          	<h4 class="star"><span>Enquiry</span> Question</h4>
            <div class="clr"></div>
            <ul class="sb_menu">
              <li><a href="${pageContext.request.contextPath}/enquiry">Home</a></li>
              <li class="active"><a href="${pageContext.request.contextPath}/enquiry/faq">FAQ</a></li>
            </ul>
          </div>
          <div class="gadget">
          	<h4 class="star"><span>About</span> us</h4>
            <div class="clr"></div>
            <ul class="sb_menu">
              <li class="active">
              <li><a href="#">Contact Us</a></li>
              <li><a href="#">Our Location</a></li>
            </ul>
          </div>
          <div class="gadget">
          	<h4 class="star"><span>Key</span> Links</h4>
            <div class="clr"></div>
            <ul class="sb_menu">
              <li class="active">
              <li><a href="#">Faculty of Engineering & IT</a></li>
              <li><a href="#">Faculty Handbook</a></li>
              <li><a href="#">CUSP</a></li>
            </ul>
          </div>
        </div>
        <div class="clr"></div>
      </div>
    </div>
  </div>
  <div class="fbg">
    <div class="fbg_resize">
      <div class="col c1">
        <h2><span>Image Gallery</span></h2>
        <a href="#"><img src="images/pic_1.jpg" width="58" height="58" alt="" /></a> <a href="#"><img src="images/pic_2.jpg" width="58" height="58" alt="" /></a> <a href="#"><img src="images/pic_3.jpg" width="58" height="58" alt="" /></a> <a href="#"><img src="images/pic_4.jpg" width="58" height="58" alt="" /></a> <a href="#"><img src="images/pic_5.jpg" width="58" height="58" alt="" /></a> <a href="#"><img src="images/pic_6.jpg" width="58" height="58" alt="" /></a> </div>
      <div class="col c2">
        <h2><span>Lorem Ipsum</span></h2>
        <p>Lorem ipsum dolor<br />
          Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec libero. Suspendisse bibendum. Cras id urna. <a href="#">Morbi tincidunt, orci ac convallis aliquam</a>, lectus turpis varius lorem, eu posuere nunc justo tempus leo. Donec mattis, purus nec placerat bibendum, dui pede condimentum odio, ac blandit ante orci ut diam.</p>
      </div>
      <div class="col c3">
        <h2><span>About</span></h2>
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec libero. Suspendisse bibendum. Cras id urna. Morbi tincidunt, orci ac convallis aliquam, lectus turpis varius lorem, eu posuere nunc justo tempus leo. llorem, eu posuere nunc justo tempus leo. Donec mattis, purus nec placerat bibendum. <a href="#">Learn more...</a></p>
      </div>
      <div class="clr"></div>
    </div>
  </div>
</div>
<div class="footer">
  <div class="footer_resize">
    <p class="lf">&copy;  University of Sydney - School of Information technology.</p>
    <div class="clr"></div>
  </div>
</div>
</html>
