<%@ include file="/WEB-INF/views/include.jsp"%>
<html>
<head>
	<%@ include file="/WEB-INF/views/header.jsp"%>
	<script type="text/javascript" src="<c:url value='/resources/js/jquery.validate.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/resources/js/validator.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/resources/js/enquire.js'/>"></script>
</head>
<body>
<div class="main">
  <div class="main_resize">
    <div class="header">
    	<%@ include file="/WEB-INF/views/head.jsp"%>
    </div>
    <div class="content">
      <div class="content_bg">
       <% Object user = session.getAttribute("user");%>
        <div class="mainbar">
<!--           <div class="article"> -->
<!--             <div class="clr"></div> -->
<%--             <c:if test="${isAddedSuc}"> --%>
<!--           		<class="post-data"> Well Done! Your Planner has been accomplished. The next step is email reminder setting.</p> -->
<%--             </c:if> --%>
<!--             <div class="clr"></div> -->
<!--           </div> -->
          <div class="article">
              <ol>
               <li>
                <label for="email">Login ID (Email) : <font color="blue"><c:out value="${user.email}"/></font></label>
<!--                   <input id="email" name="email" type="text" class="text" /> -->
<%-- 	              <input type="hidden" name="email" value="${user.email}"/> --%>
               </li>
              </ol>
            <form:form modelAttribute="planner" method="POST" action="plannerConfirm" id="plannerForm">
              <ol>

<!--                 <li> -->
<!--                   <label for="gender">Gender <font class="red">*</font></label> -->
<!--                   <input id="gender" name="gender" type="text" class="text" /> -->
<!--                 </li> -->

                <li>
                  <label for="gender">Gender <font class="red">*</font></label>
                  <select name="gender">
                    <option value="">Select</option>
	                <option value="male">Male</option>
                    <option value="female">Female</option>
	              </select>
                </li>
<!--                   <input id="email" name="email" type="text" class="text" value=${user.email} /> -->
                <li>
                  <label for="Height">Height (cm) <font class="red">*</font></label>
                  <input id="height" name="height" type="text" class="text" />
<!--                   <input id="height" name="height" type="text" class="text" value=${model.planner.height} /> -->
                </li>
                <li>
                  <label for="currentWeight">Current Weight (Kg.) <font class="red">*</font></label>
                  <input id="currentWeight" name="currentWeight" type="text" class="text" />
                </li>
                <li>
                  <label for="goalWeight">Goal Weight (Kg.) <font class="red">*</font></label>
                  <input id="goalWeight" name="goalWeight" type="text" class="text" />
                </li>
                <li>
                  <label for="duration">Plan Duration (Week)<font class="red">*</font></label>
                  <input id="duration" name="duration" type="text" class="text" />
                </li>                                                
                <li>
                  <input value="Submmit" type="submit" name="imageField" id="imageField" src="${pageContext.request.contextPath}/resources/images/submit.gif" class="send" />
                  <div class="clr"></div>
                </li>
              </ol>
            </form:form>
          </div>
          <div class="pagenavi"><span class="pages"> </span></div>
        </div>
        <%@ include file="/WEB-INF/views/menu.jsp"%>
        <script type="text/javascript">$("#PLANNER").addClass("active")</script>
        <div class="clr"></div>
      </div>
    </div>
  </div>
  <div class="fbg">
    <div class="fbg_resize">
      <div class="clr"></div>
    </div>
  </div>
</div>
<div class="footer">
  <div class="footer_resize">
    <p class="lf">&copy;  University of Sydney - School of Information technology.</p>
    <div class="clr"></div>
  </div>
</div>
</html>
