<%@ include file="/WEB-INF/views/include.jsp"%>
<html>
<head>
	<%@ include file="/WEB-INF/views/header.jsp"%>
	<script type="text/javascript" src="<c:url value='/resources/js/jquery.validate.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/resources/js/validator.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/resources/js/enquire.js'/>"></script>
</head>
<body>
<div class="main">
  <div class="main_resize">
    <div class="header">
    	<%@ include file="/WEB-INF/views/head.jsp"%>
    </div>
    <div class="content">
      <div class="content_bg">
        <br>
        <br>
        <br>
        <br>
<!--         <div class="mainbar"> -->
<!--           <div class="article"> -->
<!--             <div class="clr"></div> -->
<%--             <c:if test="${isAddedSuc}"> --%>
<!--           		<class="post-data"> Congratulations! Register Complete. Let us start to set your planner!</p> -->
<%--             </c:if> --%>
<!--             <div class="clr"></div> -->
<!--           </div> -->
          <h3 style="text-align:center">Account password reset</h3>
<!--             <div style="text-align:center" class="article"> -->
          <div style="text-align:center" class="article">
<%--             <form:form modelAttribute="register" method="POST" action="registerConfirm" id="registerForm"> --%>
            <form:form method="POST" action="resetPassword" id="resetForm">
              <ol>
              	<li>
                  <label for="email">Email </label>
                  <input id="email" name="email" type="text" class="text" />
                </li>
<!--                 <li> -->
<!--                   <label for="password">Password <font class="red">*</font></label> -->
<!--                   <input id="password" name="password" type="password" class="text" /> -->
<!--                 </li> -->
<!--                 <li> -->
<!--                   <label for="confirmPassword">Confirm Password <font class="red">*</font></label> -->
<!--                   <input id="confirmPassword" name="confirmPassword" type="password" class="text" /> -->
<!--                 </li> -->
<!--                 <li> -->
<!--                   <label for="name">Name <font class="red">*</font></label> -->
<!--                   <input id="userName" name="userName" type="text" class="text" /> -->
<!--                 </li> -->
                <li>
<!--                     <label for="password">Password</label> -->
<!--                     <input id="password" name="password" type="password" class="text" /> -->
                  <font color="red"><c:out value="${model.error}" /></font>
<%--                     <font color="red"><c:out value="${model.input.password}" /></font> --%>
                </li>
                <li>
                  <input value=" Reset " type="submit" name="imageField" id="imageField" src="${pageContext.request.contextPath}/resources/images/submit.gif" class="send" />
                  <div class="clr"></div>
                </li>
              </ol>
            </form:form>

            <form:form method="GET" action="${pageContext.request.contextPath}/" id="welcomeForm">
              <ol>
                <li>
                  <input value="     Back    " type="submit" name="imageField" id="imageField" src="${pageContext.request.contextPath}/resources/images/submit.gif" class="send" />
                  <div class="clr"></div>
                </li>
              </ol>
            </form:form>
            
            
            
            
            
            
            
          </div>
          <div class="pagenavi"><span class="pages"> </span></div>
<!--         </div> -->
<%--         <%@ include file="/WEB-INF/views/menu.jsp"%> --%>
<!--         <script type="text/javascript">$("#REGISTER").addClass("active")</script> -->
<!--         <div class="clr"></div> -->
      </div>
    </div>
  </div>
  <div class="fbg">
    <div class="fbg_resize">
      <div class="clr"></div>
    </div>
  </div>
</div>
<div class="footer">
  <div class="footer_resize">
    <p class="lf">&copy;  University of Sydney - School of Information technology.</p>
    <div class="clr"></div>
  </div>
</div>
</html>
