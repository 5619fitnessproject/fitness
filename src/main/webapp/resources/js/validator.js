
$(document).ready(function() {
	$("#enquireForm").validate({
		rules: {
			sid:{
				required: true,
				maxlength: 9,
				number: true,
				minlength: 9
			},
			email: {
				required: true,
				minlength: 5,
				maxlength: 10
			},
			firstName: {
				required: true,
				maxlength: 30,
				minlength: 1
			}, 
			question: {
				required: true
			}
		},
		messages: {
			sid:{
				required: "Please type a Student Id.",
				minlength: "SID's length is {0}.",
				number: "SID's should be number.",
				maxlength: "SID's length is {0}."
			},
			email:{
				required: "Please type a email.",
				minlength: "Please type a UniKey. It is not a whole email account.",
				maxlength: "Please type a UniKey. It is not a whole email account."
			},
			firstName: {
				required: "Please type a First name.",
				maxlength: "First name's maxlength is {0}",
				minlength: "First name's minlength is {0}"
			}, 
			question: {
				required: "Please type a question.",
				maxlength: "The question's maxlength is {0}"
			}
		},
		errorPlacement: function(error, element) {
			error.css({"float":"none" , "font-weight":"normal" , "padding":"2px 10px" , "width":"350px" , "color":"#FF7F50"});
			var e_id = $(element).attr("id");
			if (e_id == "first_name" || e_id == "last_name") {
				error.appendTo($("#errorSpanForName"));  
			}
			else{
				error.appendTo(element.parent());  
			}
		}
	})
})