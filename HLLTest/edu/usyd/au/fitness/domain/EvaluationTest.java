package edu.usyd.au.fitness.domain;

import java.util.Date;

import junit.framework.TestCase;


import edu.usyd.au.fitness.domain.Evaluation;

public class EvaluationTest extends TestCase {

    private Evaluation evaluation;

    protected void setUp() throws Exception {
    	evaluation = new Evaluation();
    }

    public void testSetAndGetUserName() {
        String testUserName = "testUser";
        assertNull(evaluation.getUserName());
        evaluation.setUserName(testUserName);
        assertEquals(testUserName, evaluation.getUserName());
    }
    
    public void testSetAndGetDate() {
        Date testDate = new Date();
        assertNull(evaluation.getDate());
        evaluation.setDate(testDate);
        assertEquals(testDate, evaluation.getDate());
    }
    
    public void testSetAndGetPlanRate() {
        String testPlanRate = "testPlanRate";
        assertNull(evaluation.getPlanRate());
        evaluation.setPlanRate(testPlanRate);
        assertEquals(testPlanRate, evaluation.getPlanRate());
    }
    
    public void testSetAndGetExerciseType() {
        String testExerciseType = "testExerciseType";
        assertNull(evaluation.getExerciseType());
        evaluation.setExerciseType(testExerciseType);
        assertEquals(testExerciseType, evaluation.getExerciseType());
    }
    
    public void testSetAndGetGoalReach() {
        String testGoalReach = "testGoalReach";
        assertNull(evaluation.getGoalReach());
        evaluation.setGoalReach(testGoalReach);
        assertEquals(testGoalReach, evaluation.getGoalReach());
    }

    public void testSetAndGetExerciseHour() {
        int testExerciseHour = 10;
        assertEquals(0, 0, 0);    
        evaluation.setExerciseHour(testExerciseHour);
        assertEquals(testExerciseHour,evaluation.getExerciseHour() , 0);
    }
    
    public void testSetAndGetCaloriesIntake() {
        int testCaloriesIntake = 10;
        assertEquals(0, 0, 0);    
        evaluation.setCaloriesIntake(testCaloriesIntake);
        assertEquals(testCaloriesIntake,evaluation.getCaloriesIntake() , 0);
    }
    
    public void testSetAndGetSittingTime() {
        int testSittingTime = 10;
        assertEquals(0, 0, 0);    
        evaluation.setSittingTime(testSittingTime);
        assertEquals(testSittingTime,evaluation.getSittingTime() , 0);
    }
    public void testSetAndGetSleepingTime() {
        int testSleepingTime = 10;
        assertEquals(0, 0, 0);    
        evaluation.setSleepingTime(testSleepingTime);
        assertEquals(testSleepingTime,evaluation.getSleepingTime() , 0);
    }
   
  
  
}
